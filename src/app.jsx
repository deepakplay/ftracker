import { useEffect, useState } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from './utils/firebase';

import Home from './pages/home/home';
import Login from './pages/login/login';
import SignUp from './pages/signup/signup';
import MainNav from './components/main-nav/main-nav';
import { useAuthContext } from './context/AuthContext';
import { Loading } from './components/loading/loading';

export const App = () => {
    const { user, createUser } = useAuthContext();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        let unsubscribe = onAuthStateChanged(auth, (user) => {
            createUser(user);
            setLoading(false);
        });

        return unsubscribe;
    }, [setLoading, createUser]);


    return (
        <>
            {loading && <Loading />}
            {
                !loading &&
                <Routes>
                    <Route path='/' element={<MainNav />}>
                        <Route index element={user ? <Home /> : <Navigate to="/login" replace />} />
                        <Route path='/login' element={!user ? <Login /> : <Navigate to="/" replace />} />
                        <Route path='/signup' element={!user ? <SignUp /> : <Navigate to="/" replace />} />
                    </Route>
                </Routes>
            }
        </>
    );
}