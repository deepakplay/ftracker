import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { signUpUser } from '../../utils/firebase';
import './signup.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
    cpassword: '',
    displayName: '',
};


const SignUp = () => {
    const [formData, setFormData] = useState(INITIAL_STATE);
    const navigate = useNavigate();

    const [submit, setSubmit] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSubmit(true);

        let userData = await signUpUser(formData);
        if (userData.success) {
            setFormData(INITIAL_STATE);
            navigate('/');
        } else {
            console.log(userData.msg);
        }
        setSubmit(false);
    }

    const handleChange = (e) => {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        setFormData((preState) => ({
            ...preState,
            [inputName]: inputValue
        }));
    }

    return (
        <div className='signup-page'>
            <form id="signup_form" onSubmit={handleSubmit}>
                <h2 className='signup_form_header'>Signup</h2>

                <div className='form-input'>
                    <label htmlFor="displayName">Username:</label>
                    <input type="text" name="displayName" id="displayName" value={formData.displayName} onChange={handleChange} />
                </div>

                <div className='form-input'>
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" value={formData.email} onChange={handleChange} />
                </div>

                <div className='form-input'>
                    <label htmlFor='password'>Password:</label>
                    <input type="password" name="password" id="password" value={formData.password} onChange={handleChange} />
                </div>

                <div className='form-input'>
                    <label htmlFor='cpassword'>Confirm Password:</label>
                    <input type="password" name="cpassword" id="cpassword" value={formData.cpassword} onChange={handleChange} />
                </div>
                {false && <span className='error-msg'>Invalid error</span>}
                <div className='form-input'>
                    <input type="submit" value={submit ? 'Loading...' : 'Signup'} className='btn' disabled={submit} />
                </div>
            </form>
        </div>
    );
}
export default SignUp;