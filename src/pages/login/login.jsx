import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { signInUser } from '../../utils/firebase';
import './login.scss';

const INITIAL_STATE = {
    email: '',
    password: ''
}

const Login = () => {
    const [formData, setFormData] = useState(INITIAL_STATE);
    const [submit, setSubmit] = useState(false);
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();

        setSubmit(true);
        let userData = await signInUser(formData);

        if (userData.success) {
            setFormData(INITIAL_STATE);
            navigate('/');
        } else {
            console.log(userData.msg);
        }
        setSubmit(false);
    }

    const handleChange = (e) => {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        setFormData((preState) => ({
            ...preState,
            [inputName]: inputValue
        }));
    }


    return (
        <div className='login-page'>
            <form id="login_form" onSubmit={handleSubmit}>
                <h2 className='login_form_header'>Login</h2>

                <div className='form-input'>
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" value={formData.email} onChange={handleChange} />
                </div>

                <div className='form-input'>
                    <label htmlFor='password'>Password:</label>
                    <input type="password" name="password" id="password" value={formData.password} onChange={handleChange} />
                </div>

                {false && <span className='error-msg'>Invalid error</span>}
                <div className='form-input'>
                    <input type="submit" value={submit ? 'Loading...' : 'Login'} className='btn' disabled={submit} />
                </div>
            </form>
        </div>
    );
}
export default Login;