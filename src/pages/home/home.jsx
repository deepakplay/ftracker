import { useState } from 'react';
import { useAuthContext } from '../../context/AuthContext';
import useCollection from '../../utils/useCollection';
import useFirestore from '../../utils/useFirestore';
import './home.scss';

const INITIAL_STATE_FORM = {
    name: '',
    amount: '',
}

const Home = () => {
    const [formData, setFormData] = useState(INITIAL_STATE_FORM);
    const [submit, setSubmit] = useState(false);
    const [form_error, setFormError] = useState('');
    const { user } = useAuthContext();
    const { addData, deleteData } = useFirestore('transactions');
    const { documents } = useCollection(
        'transactions',
        ['uid', '==', user.uid],
        ['createdAt', 'desc']);



    const handleChange = (e) => {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        setFormData((preState) => ({
            ...preState,
            [inputName]: inputValue
        }));
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSubmit(true);
        console.log(formData);
        let response = await addData(formData);

        if (response.success) {
            setFormData(INITIAL_STATE_FORM);
            if (form_error) {
                setFormError('');
            }
        } else {
            setFormError(response.message);
        }
        setSubmit(false);
    }

    return (
        <div className='home_container'>
            <div className='transaction_list_container'>
                <h3 className='transaction_heading'>Transaction List</h3>
                <div className='transaction_list'>
                    {
                        documents.map((transaction) => {
                            return (
                                <div className='transaction_item' key={transaction.id}>
                                    <span className='transaction_name'>{transaction.name}</span>
                                    <span className='transaction_amount'>{transaction.amount} Rs</span>
                                    <span className='close_button' onClick={() => deleteData(transaction.id)}>
                                        <span className='close_child'>
                                            &times;
                                        </span>
                                    </span>
                                </div>);
                        })
                    }

                </div>
            </div>
            <div className='transaction_manager'>
                <h3 className='transaction_heading'>Add a Transaction</h3>
                <form id="transaction_form" onSubmit={handleSubmit}>
                    <div className='form-input'>
                        <label htmlFor="transaction_name">Transaction Name:</label>
                        <input type="text" name="name" id="transaction_name" value={formData.name} onChange={handleChange} />
                    </div>

                    <div className='form-input'>
                        <label htmlFor='transaction_amount'>Amount:</label>
                        <input type="number" name="amount" id="transaction_amount" value={formData.amount} onChange={handleChange} />
                    </div>

                    {form_error && <span className='error-msg'>{form_error}</span>}
                    <div className='form-input'>
                        <input type="submit" value={submit ? 'Adding...' : 'Add Transaction'} className='btn' disabled={submit} />
                    </div>
                </form>
            </div>
        </div>
    );
}
export default Home;