import { useEffect, useRef, useState } from "react";
import { firestore } from "./firebase";
import { collection, onSnapshot, query, where, orderBy } from "firebase/firestore";


const useCollection = (collection_name, _queries = [], _order = []) => {
    const [documents, setDocuments] = useState([]);
    const [errors, setErrors] = useState();
    const queries = useRef(_queries);
    const order = useRef(_order);


    useEffect(() => {
        let ref = collection(firestore, collection_name);
        let query_data = [];

        if (queries.current.length) {
            query_data.push(where(...queries.current));
        }

        if (order.current.length) {
            query_data.push(orderBy(...order.current));
        }

        if (query_data.length) {
            ref = query(ref, ...query_data);
        }

        onSnapshot(ref, (snapshot) => {
            let results = [];

            snapshot.forEach(doc => {
                results.push({ id: doc.id, ...doc.data() });
            });            
            setDocuments(results);
            setErrors(null);
        }, (error) => {
            setErrors(error);
        })
    }, [collection_name, queries, order]);

    return { documents, errors };
}
export default useCollection;