import { firestore, auth } from "./firebase";
import { collection, addDoc, Timestamp, deleteDoc, doc } from "firebase/firestore";

const useFirestore = (collection_name) => {
    const colRef = collection(firestore, collection_name);
    const addData = async (data) => {
        try {
            addDoc(colRef, {
                uid: auth.currentUser.uid,
                ...data,
                createdAt: Timestamp.fromDate(new Date())
            });

            return {
                success: true
            }
        } catch (e) {
            return {
                message: e.message,
                success: false
            }
        }
    }

    const deleteData = async (id) => {
        try {
            const docRef = doc(colRef, id);
            await deleteDoc(docRef);

            return {
                success: true
            }
        } catch (e) {
            return {
                message: e.message,
                success: false
            }
        }
    }

    return { addData, deleteData };
}

export default useFirestore;