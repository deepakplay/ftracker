import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { getFirestore } from "firebase/firestore";


const firebaseConfig = {
    apiKey: "AIzaSyBtO_aCxblx3ZtPU7pwXSXI_x7DZYFnlS0",
    authDomain: "ftracker-52d7f.firebaseapp.com",
    projectId: "ftracker-52d7f",
    storageBucket: "ftracker-52d7f.appspot.com",
    messagingSenderId: "865546456124",
    appId: "1:865546456124:web:87a0e401306102b9d60739"
};


const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const firestore = getFirestore(app);

export const signUpUser = async (data) => {
    if (!data) return;
    try {
        let userData = await createUserWithEmailAndPassword(auth, data.email, data.password);
        if (userData) {
            await updateProfile(userData.user, {
                displayName: data.displayName
            });
        }

        return {
            success: true,
            user: userData.user
        }
    } catch (e) {
        return {
            success: false,
            msg: e.message
        }
    }
}


export const signInUser = async (data) => {
    if (!data) return;
    try {
        let userData = await signInWithEmailAndPassword(auth, data.email, data.password);
        return {
            success: true,
            user: userData.user
        }
    } catch (e) {
        return {
            success: false,
            msg: e.message
        }
    }
}


export const signOutUser = async () => {
    await signOut(auth);
}