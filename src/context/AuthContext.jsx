import { createContext, useContext, useReducer, useCallback } from "react";
import { AUTH_CONSTANTS } from "./AuthTypes";

const INITIAL_STATE = {
    user: null
}

const AuthContext = createContext({
    ...INITIAL_STATE
});

export const useAuthContext = () => {
    return useContext(AuthContext);
}

export const AuthContextProvider = ({ children }) => {
        
    const [state, dispatch] = useReducer((state, action) => {
        const { type, payload } = action;
        switch (type) {
            case AUTH_CONSTANTS.CREATE_USER:
                return {
                    ...state,
                    user: payload,
                }
            default:
                return state
        };

    }, INITIAL_STATE);

    const createUser = useCallback((user) => {        
        dispatch({ type: AUTH_CONSTANTS.CREATE_USER, payload: user });
    },[]);    

    return <AuthContext.Provider value={{ ...state, createUser }}>
        {children}
    </AuthContext.Provider>;
}