import './loading.scss';

export const Loading = () => {
    return (
        <div className="loading_page">
            <div className='loader_container'>
                <span className='loader'></span>
                <span>Please wait...</span>
            </div>
        </div>
    );
}