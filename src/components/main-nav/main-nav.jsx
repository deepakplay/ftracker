import React from "react";
import { Link, Outlet } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";
import { signOutUser } from "../../utils/firebase";

import './main-nav.scss';


const MainNav = () => {
    const { user } = useAuthContext();
    return (
        <>
            <div className="main-nav">
                <span className="logo-brand">
                    <Link to='/'>Finance Tracker</Link>
                </span>
                <span className="menu-items">
                    {
                        !user &&
                        <>
                            <Link className="menu_item" to='/login'>Login</Link>
                            <Link className="menu_item" to='/signup'>Signup</Link>
                        </>
                    }

                    {
                        user &&
                        <>
                            <span className="menu_item" onClick={signOutUser}>Logout</span>
                        </>
                    }
                </span>
            </div>
            <Outlet />
        </>);
}
export default MainNav;